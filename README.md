﻿
My file is: SQLQuery1.sql

exec: exec challenge565 '201010110920'



CREATE TABLE [dbo].[tuesday2](
	[ticker] [varchar](4) NOT NULL,
	[date] [varchar](12) NOT NULL,
	[open] [decimal](8, 4) NOT NULL,
	[high] [decimal](8, 4) NOT NULL,
	[low] [decimal](8, 4) NOT NULL,
	[close] [decimal](8, 4) NOT NULL,
	[vol] [int] NULL
) ON [PRIMARY]


File name:  SQLQuery4.sql

exec:exec HRange

CREATE TABLE [dbo].[wednesday](
	[Date] [date] NOT NULL,
	[Time] [varchar](4) NOT NULL,
	[Open] [decimal](5, 2) NOT NULL,
	[High] [decimal](5, 2) NOT NULL,
	[Low] [decimal](5, 2) NOT NULL,
	[Close] [decimal](5, 2) NOT NULL,
	[Volume] [varchar](7) NOT NULL
) ON [PRIMARY]