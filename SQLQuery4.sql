create procedure HRange
As
--table one
select distinct top 3 [Date], abs([open]-[close]) As Range
into #mytemptable
from Wednesday
order by Range desc
 
--table two
Select [date] , max (high) as MaxHigh
into #mytemptable2
from wednesday 
where [date] in (select [date] from #mytemptable)
group by [date];

--table three
select wednesday.[Date],[Time] as time
into #temp3
from wednesday,#mytemptable2
where wednesday.[Date] =#mytemptable2.[Date] 
and #mytemptable2.[MaxHigh]=wednesday.[High]
Order by [Time]asc

select distinct top 4
 #mytemptable.[date] as date, #mytemptable.Range, #temp3.time as 'maxium price'
from  #temp3  left join #mytemptable on 
 #temp3.[Date] =#mytemptable.[Date]

drop table #mytemptable
drop table #mytemptable2
drop table #temp3

drop procedure HRange
exec HRange