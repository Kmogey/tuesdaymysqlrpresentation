--select * from tuesday2
drop procedure challenge565
go
create procedure challenge565
@selected_date varchar(12) 
AS
Select ticker, 
sum (([close]*vol) /vol)
AS vol_weighted_Price, substring(@selected_date,7,2)+ '/'+substring(@selected_date,5,2)+'/'+substring(@selected_date,1,4)
As Date,
'START: (' + substring(convert(varchar(12),convert(datetime,
(substring(@selected_date,1,8)+' '+
substring (@selected_date,9,2)+ ':'+substring(@selected_date,11,2) )),108),1,5) +
') - END:(' + 
substring(convert(varchar(12),DATEADD(hh,+5,convert(datetime,(substring(@selected_date,1,8)+ ' '+substring(@selected_date,9,2)+ ':'+substring(@selected_date,11,2)))),108),1,5)+')'
AS interval 
From tuesday2
WHERE convert(datetime,substring([date],1,8)+' '+
substring([date],9,2)+':'+substring([date],11,2))

BETWEEN
CONVERT (datetime,substring (@selected_date,1,8)+ ' '+substring(@selected_date,9,2)+ ':'+substring(@selected_date,11,2)) AND dateadd(hh,+5,convert(datetime,substring(@selected_date,1,8)+' '+
substring(@selected_date,9,2)+ ':'+substring(@selected_date,11,2)))
group by ticker
go


 exec challenge565 '201010110920'


